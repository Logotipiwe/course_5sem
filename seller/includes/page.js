

$(document).ready(()=>{

    let fetch_orders = (search_str = $('#order_search').val())=> {
        fetch('../back/api.php?method=get_order_list&search_str='+search_str+'&loc_group=seller', {
            credentials: "include",
            headers: {
                'Content-type': 'application/json'
            }
        }).then(x => x.json())
            .then(res => {
                console.log(res);
                if (Object.keys(res).length) {
                    $('#empty_os').remove();
                    $('.os_item').remove();
                    for (let key in res) {
                        let items = '';
                        res[key]['products'].forEach(i => items += "<div class='os_list_item'>" + i.name + " <span>" + i.gramm + "</span>гр. <span>" + i.price + "</span>р.</div>");

                        let sum = res[key]['products'].reduce((x, i) => x + parseInt(i.price), 0);


                        $('#order_story').append('<div class="os_item">' +
                            '                <div class="os_head">' +
                            '                    <h3>Номер заказа: <span>' + key + '</span></h3>' +
                            '                    <div class="os_status">Статус: <span style="color: ' + ((!res[key].status) ? 'red">Не оплачено' : 'green">Оплачено') + '</span></div>' +
                            '                </div>' +
                            '                <div class="os_customer">Покупатель: <span>'+((res[key].customer !== '')?res[key].customer:'-')+'</span></div>' +
                            '                <div class="os_list">' + items + '</div>' +
                            '                <div class="os_foo">' +
                            '                   <div>Итог: <span>' + sum + '</span>р.</div>' +
    ((!res[key].status) ?   '                   <div class="confirm_order" data-order="' + key + '">Подтвердить оплату</div>':'')+
    ((!res[key].status) ?   '                   <div class="close_order" data-order="' + key + '">Отменить заказ</div>' : '') +
                            '                </div>' +
                            '            </div>')
                    }
                } else {
                    $('#order_story').html("<div id='empty_os'>У вас нет заказов</div>");
                }
            });
    };

    let fetch_products = ()=>{
        fetch('../back/api.php?method=seller_get_production',{
            credentials: "include",
        }).then(x=>x.json()).then(res=>{
            console.log(res);
            if(res.ok){
                let ans = res.ans;
                $('#seller_prod_list').html(Object.keys(ans).map(cat_id=>"" +
                    "<div class='category_item' data-id='"+cat_id+"'>" +
                    "        <h3>"+ans[cat_id].title+"</h3>" +
(ans[cat_id].products?(ans[cat_id].products.map(prod=>"" +
                    "       <div class='production_item'>" +
                    "            <div class='pi_title'>"+prod.id+'. '+prod.name+"</div>" +
                    "            <div class='pi_info' data-id='"+prod.id+"'>" +
                    "                <div class='pi_sum'>всего на складе: <span>"+prod.sum_gramm+"</span>гр. (факт.<span>"+prod.sum_gramm_fact+"</span>гр.)</div>" +
                    "                <div class='pi_admission'>Поставка: <input class='pi_ad_inp' placeholder='Грамм'><div class='pi_ad_sub' data-id='"+prod.id+"'>+</div></div>" +
                    "                <div class='pi_cost'>Стоимость:<input class='pi_cost_inp' value='"+prod.cost+"'></div>" +
                    "                <div class='pi_price'>Цена:<input class='pi_price_inp' value='"+prod.price+"'></div>" +
                    "            </div>" +
                    "        </div>").join('')):"")+
                    "    </div>"));

                $('#seller_prod_list').append("<div class='new_item'>+ новая категория <input placeholder='название'><div class='new_cat_sub'>+</div></div>")
                $('.category_item').append(
                    "<div class='production_item_new'>" +
                    "   <div>" +
                    "       + новый товар " +
                    "       <input class='ni_name' placeholder='Название'>" +
                    "   </div>" +
                    "   <div>" +
                    "       <input class='ni_cost' placeholder='Стоимость'>" +
                        "   <input class='ni_price' placeholder='Цена'>" +
                        "   <input class='ni_img' type='file'>" +
                        "   <div class='new_prod_sub'>+</div>" +
                    "   </div>" +
                    "</div>")
            } else {
                console.log('ошибка');
            }
        })
    };

    fetch_orders();

    fetch_products();

    $( "#acc_form1" ).on( "submit", function( event ) {
        event.preventDefault();
        let str = $(this).serialize();
        fetch('../back/api.php?method=set_user_data&'+str,{
            credentials: "include"
        })
            .then(x=>x.json())
            .then(res=>{
                if(res.ok){
                    alert("Данные усешно обновлены!");
                } else {
                    if(res.err = "empty_field"){
                        alert("Заполните все поля")
                    } else {
                        alert("Ошибка")
                    }
                }
            })
    });

    $("#close_sessions").on("click",()=>{
        fetch('../back/api.php?method=exit_others_devices',{
            credentials:"include"
        })
            .then(x=>x.json())
            .then(res=>{
                if(res.ok){alert("Сеансы завершены")}
                else {alert("Ошибка")}
            })
    });

    $( "#acc_form2" ).on( "submit", function( event ) {
        event.preventDefault();
        let str = $(this).serialize();
        fetch('../back/api.php?method=set_new_password&'+str)
            .then(x=>x.json())
            .then(res=>{
                console.log(res);
                if(res.ok){
                    document.cookie = 'token='+res.new_token+"; path=/course";
                    alert('Пароль успешно изменён!')
                } else{
                    alert('Ошибка!')
                }
            });
    });

    $('#order_search').on('input',()=>{
        fetch_orders()
    });

    $('#order_story')
        .on('click','.close_order',e=>{
        let order_id = e.target.dataset.order;
        fetch('../back/api.php?method=order_del&order_id='+order_id,{
            credentials: "include",
            redirect: "follow"
        }).then(x=>x.json()).then(res=>{
            console.log(res);
        });
        fetch_orders();
        fetch_products();
    })
        .on('click','.confirm_order',e=>{
            let order_id = e.target.dataset.order;
            fetch('../back/api.php?method=seller_order_confirm&order_id='+order_id,{
                credentials: "include",
                redirect: "follow"
            }).then(x=>x.json()).then(res=>{
                console.log(res);
                if(res.ok){
                    fetch_orders();
                    fetch_products();
                } else {
                    console.log(res.err_msg);
                }
            });
        });

    $('#seller_prod_list')
        .on('change','.pi_cost_inp',e=>{
        let new_val = e.target.value;
        let id = e.target.parentElement.parentElement.dataset.id;
        fetch('../back/api.php?method=seller_set_prod_data&field=cost&new_val='+new_val+'&prod_id='+id,{
            credentials: "include"
        }).then(x=>x.json()).then(res=>{
            if(res.ok){
                $(e.target).css('box-shadow','0 0 10px rgb(70,200,70)');
                setTimeout(()=>{
                    $(e.target).css('box-shadow','none');
                },1000)
            } else {
                console.log(res.err);
            }
        })
    })
        .on('change','.pi_price_inp',e=>{
            let new_val = e.target.value;
            let id = e.target.parentElement.parentElement.dataset.id;
            fetch('../back/api.php?method=seller_set_prod_data&field=price&new_val='+new_val+'&prod_id='+id,{
                credentials: "include"
            }).then(x=>x.json()).then(res=>{
                if(res.ok){
                    $(e.target).css('box-shadow','0 0 10px rgb(70,200,70)');
                    setTimeout(()=>{
                        $(e.target).css('box-shadow','none');
                    },1000)
                } else {
                    console.log(res.err);
                }
            })
        })
        .on('click','.pi_ad_sub',e=>{
            let gramm = $(e.target.parentElement).find('input').val();
            let prod_id = e.target.dataset.id;
            fetch('../back/api.php?method=new_admission&prod_id='+prod_id+'&gramm='+gramm,{
                credentials: "include"
            }).then(x=>x.json()).then(res=>{
                if(res.ok) {
                    fetch_products();
                } else{
                    console.log(res.err);
                }
            })
        })
        .on('click','.new_cat_sub',e=>{
            let title = $(e.target.parentElement).find('input').val();
            fetch('../back/api.php?method=seller_new_category&title='+title,{
                credentials: "include"
            }).then(x=>x.json()).then(res=>{
                if(res.ok){
                    fetch_products();
                } else {console.log(res.err)}
            })
        })
        .on('click','.new_prod_sub',e=>{
            let name = $(e.target.parentElement.parentElement).find('.ni_name').val();
            let cost = $(e.target.parentElement).find('.ni_cost').val();
            let price = $(e.target.parentElement).find('.ni_price').val();
            let cat_id = e.target.parentElement.parentElement.parentElement.dataset.id;

            let fd = new FormData;
            fd.append('img',$(e.target.parentElement).find('.ni_img').prop('files')[0]);

            $.ajax('../back/api.php?method=seller_new_product&name='+name+'&cat_id='+cat_id+'&cost='+cost+'&price='+price,{
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,
                success: res=>{
                    console.log((res));
                    fetch_products();
                }
            })
        })
});