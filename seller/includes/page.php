<?php
$res = DB::squery("SELECT name, surname, patronymic FROM seller where login = ?",'s',[$_COOKIE['loc_login']])->get_result();
$row = $res->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Кабинет продавца</title>
    <link href="includes/page.css" rel="stylesheet">
</head>
<body>
<script src="../libs/jquery-3.4.1.min.js"></script>
<script src="includes/page.js"></script>
<header>
    <div id="upper">
        <a href="../">На главную</a>
        <a href="../back/api.php?method=log_out"><div>Выйти</div></a>
    </div>
    <h1>Кабинет продавца <?=$_COOKIE['loc_login']?></h1>
    <div id="headline">
    </div>
</header>
<main>
    <div class="lk_part">
        <h2>Аккаунт</h2>
        <form id="acc_form1">
            <div class="inp_div">
                <p>Имя</p>
                <input name="name" placeholder="Имя" value="<?=$row['name']?>">
            </div>
            <div class="inp_div">
                <p>Фамилия</p>
                <input name="surname" placeholder="Фамилия" value="<?=$row['surname']?>">
            </div>
            <div class="inp_div">
                <p>Отчество</p>
                <input name="patronymic" placeholder="Отчество" value="<?=$row['patronymic']?>">
            </div>
            <input type="submit" class="form_sub" value="Сохранить">
        </form>
        <form id="acc_form2">
            <p>Сменить пароль</p>
            <input type="password" name="old_pass" placeholder="Старый пароль">
            <input type="password" name="new_pass" placeholder="Новый пароль">
            <input type="password" name="new_pass2" placeholder="Подтверждение">
            <input type="submit" value="Сменить пароль">
        </form>
        <div id="close_sessions">Завершить все сеансы</div>
    </div>
    <div class="lk_part">
        <h2>История заказов</h2>
        <input placeholder="Поиск по номеру" id="order_search">
        <div id="order_story"></div>
    </div>
</main>
<div id="seller_prod_list">
    <div class="category_item">
    </div>
</div>
</body>
</html>