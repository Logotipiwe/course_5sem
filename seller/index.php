<?php

require_once '../back/db.php';
if(DB::auth($_COOKIE['loc_login'],$_COOKIE['token']) === 'seller'){
    require_once 'includes/page.php';
} else {
    require_once 'login.html';
}