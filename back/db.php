<?php

class DB
{
    public static $connection = null;

    public static function connect(){
        self::$connection = new mysqli('localhost','admin','pass','course');
        self::$connection->set_charset('utf8');
    }

    public static function checkConnection()
    {
        if(!self::$connection){
            self::connect();
        }
    }

    public static function close()
    {
        if(self::$connection){
            self::$connection->close();
            self::$connection = null;
        }
    }

    public static function query($str)
    {
        self::checkConnection();

        return self::$connection->query($str);
    }

    public static function squery($str,$types = '',$args = []){
        self::checkConnection();

        $stmt = self::$connection->prepare($str);

        switch (count($args)){
            case 1:
                $stmt->bind_param($types,$args[0]);
                break;
            case 2:
                $stmt->bind_param($types,$args[0],$args[1]);
                break;
            case 3:
                $stmt->bind_param($types,$args[0],$args[1],$args[2]);
                break;
            case 4:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3]);
                break;
            case 5:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4]);
                break;
            case 6:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5]);
                break;
            case 7:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6]);
                break;
            case 8:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7]);
                break;
            case 9:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7],$args[8]);
                break;
            case 10:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7],$args[8],$args[9]);
                break;
            case 11:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7],$args[8],$args[9],$args[10]);
                break;
            case 12:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7],$args[8],$args[9],$args[10],$args[11]);
                break;
            case 13:
                $stmt->bind_param($types,$args[0],$args[1],$args[2],$args[3],$args[4],$args[5],$args[6],$args[7],$args[8],$args[9],$args[10],$args[11],$args[12]);
                break;
        }
        $stmt->execute();
//        $res = $stmt->get_result();
        return $stmt;
    }

    public static function multi_query($str){
        self::checkConnection();

        return self::$connection->multi_query($str);
    }

    public static function auth($login, $hash)
    {
        $hash = strtolower($hash);

        $res1 = self::squery("SELECT login,password,rand FROM users where login = ?",'s',[$login])->get_result();
        $res2 = self::squery("SELECT login,password,rand FROM seller where login = ?",'s',[$login])->get_result();
        $res3 = self::squery("SELECT login,password,rand FROM owner where login = ?",'s',[$login])->get_result();
        if($res1->num_rows){
            $row = $res1->fetch_assoc();
            if(md5($row['password'].$row['rand']) === $hash) return 'user'; else return 'none';
        }
        else if ($res2->num_rows){
            $row = $res2->fetch_assoc();
            if(md5($row['password'].$row['rand']) === $hash) return 'seller'; else return 'none';
        }
        else if($res3->num_rows){
            $row = $res3->fetch_assoc();
            if(md5($row['password'].$row['rand']) === $hash) return 'owner'; else return 'none';
        }
        else return 'none';
    }
}
