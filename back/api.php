<?php
require_once 'db.php';

class Methods{
    public static $withoutAuth = array('sign_up','sign_in_check','sign_in','log_out','get_production','check','check_login','check_auth','new_order');

    public static $index_file = '../';

    public static function sign_up($data){
        $rand = rand(0,9999);
        $data['pass'] = ($data['pass'] !== '')?md5($data['pass']):'';
        $data['pass2'] = md5($data['pass2']);//шифровка пароля

        $insert = [
            'login'=>           $data['login'],
            'pass'=>            $data['pass'],
            'name'=>            $data['name'],
            'surname'=>         $data['surname'],
            'rand'=>            $rand,
            'address_city'=>    $data['address_city'],
            'address_street'=>  $data['address_street'],
            'address_house'=>   (int)$data['address_house'],
            'address_apart'=>   (int)$data['address_apart'],
            'email'=>           $data['email'],
            'b_date'=>          $data['b_date'],
            'phone'=>           $data['phone']
        ]; //сбор данных в один массив для внесения
        foreach ($insert as &$item) {$item = ($item==='')?null:$item;}

        $flag = true;
        if($data['login'] === '' or $data['pass'] === '' or $data['pass']!==$data['pass2']){$flag = false;}
        //проверка на правильность ввода
        if($flag) {
            $query = "INSERT INTO users (`login`, `password`, `name`, `surname`, `rand`,`address_city`,
                      `address_street`,`address_house`,`address_apart`,`email`,`b_date`,`phone`)
                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $reg = DB::squery($query, 'ssssissiisss', array_values($insert));
            if ($reg->affected_rows > 0) {
                setcookie('loc_login', $data['login'], 0, '/course');
                setcookie('token', md5($data['pass'] . $rand), 0, '/course');
                header('Location: '.self::$index_file); //перевод на главную страницу
            }
        } //внесение пользователя в базу и установка cookie для идентификации пользователя
    }

    public static function sign_in_check($data){
        $reg = DB::squery("SELECT * FROM users WHERE login = ? AND `password` = ?",'ss',[$data['login'],md5($data['pass'])])->get_result();
        if($reg->num_rows){
            return json_encode(["ok"=>true,$data]);
        } return json_encode(["ok"=>false,$data]);
    }

    public static function sign_in($data){
        if($data['loc_group'] === 'user'){
            $table = 'users';
            $location = 'Location: '.self::$index_file;
        } else if($data['loc_group'] === 'seller'){
            $table = 'seller';
            $location = 'Location: '.$_SERVER['HTTP_REFERER'];
        } else if($data['loc_group'] === 'owner') {
            $table = 'owner';
            $location = 'Location: '.$_SERVER['HTTP_REFERER'];
        } else {
            header('Location: '.self::$index_file);
        } //авторизация и установка соответствующих ей данных

        $reg = DB::squery("SELECT * FROM $table WHERE login = ? AND `password` = ?",'ss',[$data['login'],md5($data['pass'])])->get_result();
        if($reg->num_rows){
            $reg = $reg->fetch_assoc();
            setcookie('loc_login',$reg['login'],0,'/course');
            setcookie('token',md5($reg['password'].$reg['rand']),0,'/course');
        } //если данные соответствуют - установка cookie для идентификации
        header($location); //переход на соответствующую страницу
    }

    public static function log_out(){
        setcookie('loc_login', '', 0, '/course');
        setcookie('token', '', 0, '/course');
        header('Location: ' . self::$index_file);
    }

    public static function exit_others_devices($data){
        $login = $data['loc_login'];
        $hash = $data['token'];

        if($data['gr'] === 'user') $table = 'users';
        else if($data['gr'] === 'seller') $table = 'seller';
        else if($data['gr'] === 'owner') $table = 'owner';
        else return json_encode(array("ok"=>false,"err"=>"auth_err"));

        $res = DB::squery("SELECT rand,password FROM $table where login = ?",'s',[$login])->get_result();
        $row = $res->fetch_assoc();
        $rand = ($row['rand']<10000)?rand(10000,19999):rand(0,9999);

        $changed = DB::squery("UPDATE $table SET rand = ? WHERE login = ?",'is',[$rand,$login]);
        if($changed->affected_rows) {
            setcookie('token', md5($row['password'].$rand), 0, '/course');
            return json_encode(["ok"=>true]);
        } else return json_encode(["ok"=>false,"err"=>"db_err"]);
    }

    public static function check_login($data){
        $res = DB::squery("select * from ((select login from users) UNION all (select login from seller) UNION ALL (select login from owner)) t1 WHERE login = ?",'s',[$data['login']])->get_result();
        if($res) {
            if ($res->num_rows) {
                return json_encode(array("ok" => true,"success"=>false));
            } else return json_encode(array("ok" => true,"success"=>true));
        } else return json_encode(array("ok" => false,"err"=>"sql_err","err_msg"=>"Ошибка базы данных"));
    }

    public static function get_production($data){

        $param = [];
        $param_type = '';

        if(isset($data['category'])){
            $where_cat = "AND t1.category = ?";
            $param[] = $data['category'];
            $param_type.='i';
        } else {$where_cat = "";}

        if(isset($data['search'])){
            $search = "AND t1.name like ?";
            $param[] = "%$data[search]%";
            $param_type.='s';
        } else $search = "";

        $res = DB::squery("
        select t1.product id, sum_ad-COALESCE(sum_ordered,0) gramm, t1.name name, t1.price price, t1.category, t1.description
        from (SELECT product, sum(gramm) sum_ad, products.name name, products.price price, products.category, products.description FROM admissions JOIN products ON products.id = admissions.product GROUP BY product) t1
        LEFT JOIN (SELECT prod_id product, SUM(gramm) sum_ordered from ord_prod GROUP BY product) t2 
        on t1.product = t2.product
        WHERE sum_ad-COALESCE(sum_ordered,0) > 500 $where_cat $search 
        ORDER by id
        ",$param_type,$param)->get_result();
        if($res) {
            $products = array();
            while($row = $res->fetch_assoc()){
                $products[] = $row;
            }
            return json_encode(array(
                "ok" => true,
                "list"=>$products
            ));
        } else{
            return json_encode(array(
                "ok"=>false,
                "res"=>$res
            ));
        }
    }

    public static function check($data){
        return print_r(explode(' ',''),1);
    }

    public static function check_auth($data){
        if($data['gr'] !== 'none') return 1; else return 0;
    }

    public static function new_order($data){

        if ($data['products']){
            $products = json_decode($data['products']);
        } else {
            $products = array();
        }
        $missing = array();

        foreach ($products as $product){
            $res = DB::squery("
            select t1.product id, sum_ad-COALESCE(sum_ordered,0) gramm
            from (SELECT product, sum(gramm) sum_ad FROM admissions GROUP BY product) t1
            LEFT JOIN (SELECT prod_id product,sum(gramm) sum_ordered FROM ord_prod GROUP BY product) t2
            on t1.product = t2.product
            WHERE t1.product = ?",'i',[$product->id])->get_result();
            $row = $res->fetch_assoc();
            if($row['gramm'] < $product->gramm){
                $missing[] = array(
                    "id"=>$product->id,
                    "name"=>$product->name,
                    "gramm"=>(int)$product->gramm - (int)$row['gramm']
                    );
            }
        }

        if(!count($missing) and count($products)){ //оформление заказа
            if($data['gr'] === 'user') {
                $user_id = DB::squery("SELECT id from users where login = ?",'s',[$data['loc_login']])->get_result()->fetch_assoc()['id'];


                $rand = str_split((string)time());
                shuffle($rand);
                $rand = implode('', $rand);
                $order_id = strtoupper(base_convert($rand . $user_id, 10, 32));

                $query_arr = array();

                $query_arr[] = "INSERT INTO `orders` (`id`, `timestamp`, `seller_id`) VALUES ('$order_id', CURRENT_TIMESTAMP, '0')";

                $query_arr[] = "INSERT INTO `ord_reg` (`id`, `user`) VALUES ('$order_id', $user_id)";
                foreach ($products as $product) {
                    $prod_id = (int)$product->id;
                    $price = DB::query("SELECT price from products where id = ".$prod_id)->fetch_assoc()['price'];

                    $price = round($price / 1000 * (int)$product->gramm);

                    $gramm = (int)$product->gramm;

                    $query_arr[] = "INSERT INTO `ord_prod` 
                              (`ord_id`, `prod_id`, `gramm`, `price`) VALUES 
                              ('$order_id', '$prod_id', $gramm, $price)";
                }

                $query = implode('; ', $query_arr);
                if (DB::multi_query($query)) {
                    return json_encode(array("ok" => true, "order_id" => $order_id));
                } else {
                    return json_encode(array("ok" => false, "err" => "sql_err",));
                }
            } else { //Гость

                if($data['name'] || $data['surname'] || $data['email'] || $data['b_date'] || $data['phone'] || $data['address']){

                    $uniq_id = abs((int)substr($data['phone'], -3));
                    $rand = str_split((string)time());
                    shuffle($rand);
                    $rand = implode('', $rand);
                    $order_id = strtoupper(base_convert($rand . $uniq_id, 10, 32));

                    $query_arr = array();

                    $query_arr[] = "INSERT INTO `orders` (`id`, `timestamp`, `seller_id`) VALUES ('$order_id', CURRENT_TIMESTAMP, '0')";

                    $query_arr[] = "INSERT INTO `ord_guest` (`id`, `name`,`surname`,`email`,`b_date`,`phone`,`address_city`,`address_street`,`address_house`,`address_apart`) VALUES
                                ('$order_id', '$data[name]', '$data[surname]','$data[email]','$data[b_date]','$data[phone]','$data[address_city]','$data[address_street]','$data[address_house]','$data[address_apart]')";

                    foreach ($products as $product) {
                        $prod_id = $product->id;
                        $price = DB::query("SELECT price from products where id = ".$prod_id)->fetch_assoc()['price'];

                        $price = round($price / 1000 * (int)$product->gramm);

                        $query_arr[] = "INSERT INTO `ord_prod` 
                              (`ord_id`, `prod_id`, `gramm`, `price`) VALUES 
                              ('$order_id', '$prod_id', $product->gramm, $price)";
                    }

                    $query = implode('; ', $query_arr);
                    if (DB::multi_query($query)) {
                        return json_encode(array("ok" => true, "order_id" => $order_id));
                    } else {
                        return json_encode(array("ok" => false, "err" => "sql_err",));
                    }
                } else {
                    return json_encode(array("ok"=>false,"err"=>"auth_err","data"=>$data));
                }
            }
        } else {
            if(count($products)){
                return json_encode(array("ok"=>false,"err"=>"missing","missing"=>$missing));
            } else {
                return json_encode(array("ok"=>false,"err"=>"empty"));
            }
        }
    }

    public static function set_new_password($data){
        $old_pass = $data['old_pass'];
        $new_pass = $data['new_pass'];
        $conf = $data['new_pass2'];
        if($data['gr'] === 'user') $table = 'users';
        else if($data['gr'] === 'seller') $table = 'seller';
        else if($data['gr'] === 'owner') $table = 'owner';
        else return json_encode(array("ok"=>false,"err"=>"auth_err"));

        $pass = DB::squery("SELECT password from $table where login = ?",'s',[$data['loc_login']])->get_result()->fetch_assoc()['password'];

        if($pass === md5($old_pass) and $new_pass === $conf){
            $res = DB::squery("UPDATE $table SET password = ? WHERE login = ?",'ss',[md5($new_pass),$data['loc_login']]);
            if($res->affected_rows){
                $rand = DB::squery("SELECT rand FROM $table where login = ?",'s',[$data['loc_login']])->get_result()->fetch_assoc()['rand'];
                $new_token = md5(md5($new_pass).$rand);
                return json_encode(array(
                    "ok"=>true,
                    "new_token"=>$new_token
                ));
            } else {return json_encode(array("ok"=>false,"err"=>"sql_err"));}
        } else {return json_encode(array("ok"=>false,"err"=>"input_err"));}
    }

    public static function set_user_data($data){
        $name = $data['name'];
        $surname = $data['surname'];

        if($data['gr'] === 'user'){

            $phone = $data['phone'];

            if($name !== '' and $surname !== '' and $phone !== '') $res = DB::squery("UPDATE users SET name=?, surname=?, phone=? where login=?",'ssss',[$name,$surname,$phone,$data['loc_login']]);
            else return json_encode(array("ok"=>false,"err"=>"empty_field"));

        } else if($data['gr'] === 'seller'){

            $patronymic = $data['patronymic'];

            if($name !== '' and $surname !== '' and $patronymic !== '') $res = DB::squery("UPDATE seller SET name=?, surname=?, patronymic=? where login=?",'ssss',[$name,$surname,$patronymic,$data['loc_login']]);
            else return json_encode(array("ok"=>false,"err"=>"empty_field"));

        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));

        if($res->affected_rows > 0){

            return json_encode(array("ok"=>true));

        } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
    }

    public static function get_order_list($data){
        //Удаление не подтвержденных заказов более 7 дней давности
        $res = DB::query("DELETE FROM orders WHERE id in (SELECT id FROM (SELECT id, timestamp, seller_id, datediff(now(),timestamp) day FROM orders ORDER BY `orders`.`timestamp` DESC) t1 WHERE t1.day > 7 and seller_id = 0)");

        if($data['gr'] === 'user'){
            $query = "
            SELECT ord_id order_id, products.name name, gramm, ord_prod.price price, orders.seller_id, COALESCE(login,concat(ord_guest.name,' ',ord_guest.surname)) login, orders.timestamp time FROM `ord_prod`
                LEFT JOIN orders on orders.id = ord_prod.ord_id
                JOIN products on ord_prod.prod_id = products.id
                LEFT JOIN ord_reg on ord_reg.id = orders.id
                LEFT JOIN ord_guest on  ord_guest.id = orders.id
                LEFT JOIN users on users.id = ord_reg.user
                where login = ?
                ORDER BY time DESC
            ";
            $param = [$data['loc_login']];
        } else if($data['gr'] === 'seller'){

            $query = "
            SELECT ord_id order_id, products.name name, gramm, ord_prod.price price, orders.seller_id, COALESCE(login,concat(ord_guest.name,' ',ord_guest.surname)) login, orders.timestamp time FROM `ord_prod`
                LEFT JOIN orders on orders.id = ord_prod.ord_id
                JOIN products on ord_prod.prod_id = products.id
                LEFT JOIN ord_reg on ord_reg.id = orders.id
                LEFT JOIN ord_guest on  ord_guest.id = orders.id
                LEFT JOIN users on users.id = ord_reg.user
                where ord_id like ?
                ORDER BY time DESC
            ";
            $param = ["%$data[search_str]%"];

        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
        $order_list = array();
        $res = DB::squery($query,'s',$param)->get_result();
        while($row = $res->fetch_assoc()){
            $order_list[$row['order_id']]['products'][] = array("name"=>$row['name'],"gramm"=>$row['gramm'],"price"=>$row['price']);
            $order_list[$row['order_id']]['status'] = ((int)$row['seller_id'] !== 0)?1:0;
            $order_list[$row['order_id']]['customer'] = $row['login'];
        }
        return json_encode($order_list);

    }

    public static function order_del($data){
        $res= DB::squery("DELETE FROM orders where id = ?",'s',[$data['order_id']]);
        if($res->affected_rows){
            return json_encode(array("ok"=>true));
        } else {return json_encode(array("ok"=>false,"err"=>"sql_err"));}
    }

    public static function seller_order_confirm($data){
        if($data['gr'] === 'seller') {
            $seller_id = (int)DB::squery("SELECT id from seller where login = ?", 's', [$data['loc_login']])->get_result()->fetch_assoc()['id'];
            if($seller_id and $data['order_id']){
                $res = DB::squery("UPDATE orders SET seller_id = ? where id = ?",'is',[$seller_id, $data['order_id']]);
                if($res->affected_rows > 0){
                    return json_encode(array("ok"=>true));
                } else return json_encode(array("ok"=>false,"err"=>"sql_err","err_msg"=>"Ошибка базы данных"));
            } return json_encode(array("ok"=>false,"err"=>"data_err","err_msg"=>"Ошибка данных"));
        } return json_encode(array("ok"=>false,"err"=>"auth_err","err_msg"=>"Ошибка авторизации"));
    }

    public static function seller_get_production($data){
        if($data['gr'] === 'seller') {
            $ans = array();

            $res = DB::query("SELECT categories.id cat_id, title cat_title, products.id prod_id, name, cost, price 
                              from categories
                              LEFT JOIN products on categories.id = products.category");
            while ($row = $res->fetch_assoc()) {

                $prod_id = (int)$row['prod_id'];
                $sum_gramm = DB::query("
                  select (select coalesce((SELECT sum(gramm) 
                    FROM admissions 
                    WHERE product = $prod_id 
                    GROUP BY product),0)) - 
                (select coalesce((SELECT sum(gramm) 
                    FROM ord_prod 
                    WHERE prod_id = $prod_id 
                    GROUP BY prod_id),0)) 
                sum_gramm")->fetch_assoc()['sum_gramm'];

                $sum_gramm_fact = DB::query("
                  select (select coalesce((SELECT sum(gramm) 
                    FROM admissions 
                    WHERE product = $prod_id 
                    GROUP BY product),0)) - 
                (select coalesce((SELECT sum(gramm) 
                    FROM ord_prod 
                    LEFT JOIN orders ON orders.id = ord_prod.ord_id 
                    WHERE prod_id = $prod_id and seller_id <> 0 
                    GROUP BY prod_id),0)) 
                sum_gramm_fact")->fetch_assoc()['sum_gramm_fact'];


                $ans[$row['cat_id']]['title'] = $row['cat_title'];
                if($row['prod_id'])$ans[$row['cat_id']]['products'][] = array("id"=>$row['prod_id'],"name"=>$row['name'],"cost"=>$row['cost'],
                                                                                "price"=>$row['price'],"sum_gramm"=>$sum_gramm,"sum_gramm_fact"=>$sum_gramm_fact);
            }
            return json_encode(array("ok"=>true,"ans"=>$ans));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    } //done

    public static function seller_set_prod_data($data){
        if($data['gr'] === 'seller'){
            $new_val = $data['new_val'];
            $id = (int)$data['prod_id'];
            $field = ($data['field'] === 'cost')?'cost':'price';
            if(is_numeric($new_val) and round($new_val) === (float)$new_val and (int)$new_val >= 0){
                $res = DB::squery("UPDATE products SET $field = ? WHERE id = ?",'ii',[$new_val,$id]);

                if($res) return json_encode(array("ok"=>true));
                else return json_encode(array("ok"=>false,"err"=>"sql_err"));
            } else return json_encode(array("ok"=>false,"err"=>"wrong_data"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    } //done

    public static function new_admission($data){
        if($data['gr'] === 'seller'){
            $prod_id = (int)$data['prod_id'];
            $gramm = (int)$data['gramm'];

            $cost = (int)DB::squery("SELECT cost from products where id = ?",'i',[$prod_id])->get_result()->fetch_assoc()['cost'];
            $cost = $cost/1000*$gramm;

            $res = DB::squery("INSERT INTO `admissions` (`product`, `gramm`, `cost`) VALUES (?,?,?)",'iii',[$prod_id, $gramm, $cost]);

            if($res->affected_rows > 0){
                return json_encode(array("ok"=>true));
            } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    } //done

    public static function seller_new_category($data){
        if($data['gr'] === 'seller'){
            $title = $data['title'];
            if(isset($title) and $title !== ""){
                $res = DB::squery("INSERT INTO `categories` (`title`) VALUES (?);",'s',[$title]);
                if($res){
                    return json_encode(array("ok"=>true));
                } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
            } else return json_encode(array("ok"=>false,"err"=>"wrong_data"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    }

    public static function seller_new_product($data){
        if($data['gr'] === 'seller'){

            $name = $data['name'];
            $cost = (int)$data['cost'];
            $price = (int)$data['price'];

            $cat_id = (int)$data['cat_id'];

            $img = $_FILES['img'];

            if($name and $cost and $price and $img['type'] === 'image/png' and $img['size'] < 200000){
                $res = DB::squery("INSERT INTO `products` (`category`,`name`,`cost`,`price`) VALUES (?,?,?,?)",'isii',[$cat_id,$name,$cost,$price]);
                $new_id = DB::query("SELECT LAST_INSERT_ID() new_id")->fetch_assoc()['new_id'];
                if($new_id){
                    if (move_uploaded_file($img['tmp_name'], "../img/$new_id.PNG")) {
                        return json_encode(array("ok" => true));
                    } else{
                        DB::query("DELETE FROM products WHERE id = $new_id");
                        return json_encode(array("ok"=>false,"err"=>"image_err"));
                    }
                } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
            } else return json_encode(array("ok"=>false,"err"=>"wrong_data"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    }

    public static function get_owner_data($data){
        if($data['gr'] === 'owner'){

            $ans = array();

            $date1 = $data['date1'];
            $date2 = $data['date2'];

            if(!$date1) $date1 = '2015-01-01';
            if(!$date2) $date2 = '2100-01-01';

            $query = "SELECT 
                products.id id, 
                products.name name,
                COALESCE(t2.income,0) income, 
                COALESCE(t2.sum_gramm,0) sum_gramm,  
                COALESCE(t2.income,0) - COALESCE(t1.outcome,0) profit 
            FROM `products`
            LEFT JOIN (
                select  product, sum(cost) outcome 
                FROM (select product, cost FROM admissions WHERE date(timestamp) BETWEEN ? AND ?) tt1
                GROUP BY tt1.product) t1 on t1.product = products.id
            LEFT JOIN (
                select prod_id product, sum(price) income, sum(gramm) sum_gramm from 
                (select * FROM ord_prod 
                    LEFT JOIN orders ON orders.id = ord_prod.ord_id 
                    WHERE seller_id <> 0 and date(timestamp) BETWEEN ? and ?) tt2
                GROUP BY tt2.prod_id) t2 on t2.product = products.id
            GROUP BY products.id
            ORDER BY profit DESC LIMIT 0,10";
            $res = DB::squery($query,'ssss',[$date1,$date2,$date1,$date2])->get_result();
            if($res){
                while($row = $res->fetch_assoc()) $ans['rate'][] = $row;
            }

            $query ="select (
                SELECT COALESCE(sum(price),0) income 
                FROM (SELECT ord_prod.prod_id prod_id, price 
                    FROM ord_prod 
                    LEFT JOIN orders on orders.id = ord_prod.ord_id 
                    WHERE seller_id <> 0 and date(timestamp) BETWEEN ? and ?) t1) income,
                (
                SELECT COALESCE(sum(cost),0) 
                FROM admissions 
                WHERE date(timestamp) BETWEEN ? and ?) outcome";
            $res = DB::squery($query,'ssss',[$date1,$date2,$date1,$date2])->get_result();
            if($res) $ans['money'] = $res->fetch_assoc();

            $query = "SELECT id, login, name, surname, patronymic, passport FROM seller";
            $res = DB::query($query);
            if($res){
                while($row = $res->fetch_assoc()){
                    $ans['sellers'][] = $row;
                }
            }

            return json_encode(array("ok"=>true,"ans"=>$ans));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    }

    public static function new_seller($data){
        if($data['gr'] === 'owner'){
            $login = $data['login'];
            $password = $data['password'];
            $passport = $data['passport'];

            if($login !== '' and $password !== '' and $passport){
                $rand = rand(0,10000);

                $res = DB::squery("INSERT INTO seller (login, password, passport, rand) VALUES (?, ?, ?, ?)",'sssi',[$login,md5($password),$passport,$rand]);
                if($res->affected_rows !== -1){
                    return json_encode(array("ok"=>true,'res'=>$res->affected_rows));
                } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
            } else return json_encode(array("ok"=>false,"err"=>"wrong_data"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    }

    public static function seller_delete($data){
        if($data['gr'] === 'owner'){
            $res = DB::squery("DELETE FROM seller where id = ?",'i',[$data['id']]);
            if($res->affected_rows < 1){
                return json_encode(array("ok"=>true));
            } else return json_encode(array("ok"=>false,"err"=>"sql_err"));
        } else return json_encode(array("ok"=>false,"err"=>"auth_err"));
    }
}

$data = array_merge($_GET,$_POST,$_COOKIE);
$login = $data['loc_login'];
$token = $data['token'];

$method = $data['method'];

$data['gr'] = DB::auth($login,$token);

if(is_callable(array('Methods',$method))){
    if(in_array($method,Methods::$withoutAuth) or $data['gr'] !== 'none'){
        echo call_user_func(array('Methods',$method),$data);
    } else {
        echo json_encode(array("ok"=>false,"err"=>"auth_err"));
    }
} else{
    echo json_encode(array("ok"=>false,"err"=>"method_err"));
}
