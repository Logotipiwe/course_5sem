
let fetch_prod = [];
let cart = [];

let fetch_func = (params = {})=> {
    let url = 'back/api.php?method=get_production';
    if(params.sort) url+='&sort='+params.sort;
    if(params.category) url+='&category='+params.category;
    if(params.prod_search) url+='&search='+params.prod_search;
    fetch(url, {
        credentials: "include",
        headers: {
            'Content-type': 'application/json'
        }
    }).then(x => x.json()).then(res => {
        if(res.ok){
            fetch_prod = res.list;
            html_prod(res.list);
        } else console.log(res);
    });
};

let html_prod = list=>{
    if(list.length) {
        $('#prod_list').html(list.map(item => {
            return "<div class='prod_item' id='prod_item_" + item['id'] + "' data-description='"+item['description']+"' data-price='"+item['price']+"' data-category='"+item['category']+"' data-id='" + item['id'] + "'>" +
                "           <img src='img/" + item['id'] + ".PNG'>" +
                "           <div class='item_title'>" + item['name'] + "</div>" +
                "           <select class='price_select' data-price='" + item['price'] + "'>" +
                "               <option value='50'>50 гр.</option>" +
                "               <option value='100'>100 гр.</option>" +
                "               <option value='250'>250 гр.</option>" +
                "               <option value='500'>500 гр.</option>" +
                "           </select>" +
                "           <div class='item_price'><span>" + Math.round(parseInt(item['price']) / 20) + "</span> р.</div>" +
                "           <div class='item_buy'>Купить</div>" +
                "       </div>"
        }));
    } else {
        $('#prod_list').html("<div id='empty_prod_list'>В этой категории нет доступных товаров</div>");
    }
};

let html_one_prod = (id,cat_id,title,price,desc)=>{
    $('#prod_list').html("" +
        "<div class='prod_one_item'>" +
        "   <div class='poi_main' data-id='"+id+"'>" +
        "       <img src='img/"+id+".PNG'>" +
        "       <div class='poi_main2'>" +
        "           <div class='poi_title'>"+title+"</div>" +
        "           <select class='price_select' data-price='"+price+"'><option value='50'>50 гр.</option><option value='100'>100 гр.</option><option value='250'>250 гр.</option><option value='500'>500 гр.</option></select>" +
        "           <div class='item_price'><span>" + Math.round(parseInt(price) / 20) + "</span>р.</div>" +
        "           <div class='item_buy'>Купить</div>" +
        "       </div>" +
        "   </div>" +
        "   <div class='poi_description'>"+desc+"</div>" +
        "</div>");
    let cat_num = cat_id+1;
    let cat_title = $('.menu_item:nth-child('+cat_num+')').text();
    $('#settings>p>b').text("Весь ассортимент > "+cat_title+" > "+title);
};

let get_cookie = str=>{
    let res =  document.cookie.
    split('; ').
    map(x=>x.split('=')).
    filter(x=>x[0]===str);
    if (res.length) return res[0][1];
    else return '';
};

let init_cart = ()=>{
    let cart = localStorage.getItem('cart');
    if(cart){
        cart = JSON.parse(cart);
        $('#cart_button span').text(cart.length);
        let sum = cart.reduce((x,i)=>x+i.price,0);
        $('#cart_buy span').text(sum);
        $('#cart_list').html(cart.map(item=>{
            return '<div class="cart_list_item">' +
                '    <div class="ci_img"><img src="img/'+item.id+'.PNG"></div>' +
                '    <div class="ci_data">' +
                '        <span>'+item.name+'</span>' +
                '        <div>' +
                '            <span>'+item.gramm+' гр.</span>' +
                '            <span>'+item.price+' р.</span>' +
                '        </div>' +
                '    </div>' +
                '    <div class="ci_remove">&#216;</div>' +
                '</div>'
        }));
    }
};

let add_cart = obj=>{
    let cart = [];
    try {
        cart = JSON.parse(localStorage.getItem('cart'));
        if(cart.filter(x=>x.id === obj.id).length){
            let inc_elem = cart.filter(x=>x.id === obj.id)[0];
            inc_elem.price+=obj.price;
            inc_elem.gramm+=obj.gramm;
            cart = cart.filter(x=>x.id !== obj.id);
            cart.push(inc_elem)
        } else {
            $('#cart_button span').text(parseInt($('#cart_button span').text())+1);
            cart.push(obj);
        }
    } catch (e) {
        localStorage.clear();
        $('#cart_button span').text('1');
        cart = [obj];
    }
    localStorage.setItem('cart',JSON.stringify(cart));
    $('#cart_list').html(cart.map(item=>{
        return '<div class="cart_list_item">' +
            '                <div class="ci_img"><img src="img/'+item.id+'.PNG"></div>' +
            '                <div class="ci_data">' +
            '                    <span>'+item.name+'</span>' +
            '                    <div>' +
            '                        <span>'+item.gramm+' гр.</span>' +
            '                        <span>'+item.price+' р.</span>' +
            '                    </div>' +
            '                </div>' +
            '                <div class="ci_remove">&#216;</div>' +
            '            </div>'
    }));

    let sum = cart.reduce((x,i)=>x+i.price,0);
    $('#cart_buy span').text(sum);

};

let del_cart = num=>{
    let num_child = num+1;
    try{
        let cart = JSON.parse(localStorage.getItem('cart'));
        $('.cart_list_item:nth-child('+num_child+')').remove();
        cart.splice(num,1);
        localStorage.setItem('cart',JSON.stringify(cart));
        $('#cart_button span').text(parseInt($('#cart_button span').text())-1);

        let sum = cart.reduce((x,i)=>x+i.price,0);
        $('#cart_buy span').text(sum);
    } catch (e) {
        alert("Ошибка корзины");
        localStorage.clear()
    }
};

let clear_cart = ()=>{
    localStorage.clear();
    $('.cart_list_item').remove();
    $('#cart_buy span').text('0');
    $('#cart_button span').text('0');
    $('#cart').hide();
};

$(document).ready(()=>{

    fetch_func(); //заполнение товарами
    init_cart(); //инициализация корзины из локал стореж

    $("#guest_data").click(e=>{
        if(e.target.id === "guest_data"){
            $("#guest_data").hide();
        }
    });

    $('#gd_submit').click(()=>{
        let name = $('#guest_data').find('#gd_inp_name').val();
        let surname = $('#guest_data').find('#gd_inp_surname').val();
        let email = $('#guest_data').find('#gd_inp_email').val();
        let b_date = $('#guest_data').find('#gd_inp_bdate').val();
        let phone = $('#guest_data').find('#gd_inp_phone').val();
        let address_city = $('#guest_data').find('#gd_inp_address_city').val();
        let address_street = $('#guest_data').find('#gd_inp_address_street').val();
        let address_house = $('#guest_data').find('#gd_inp_address_house').val();
        let address_apart = $('#guest_data').find('#gd_inp_address_apart').val();
        if(name && surname && email && phone){
            let guest_data = [];
            let obj = {name, surname, email, b_date, phone, address_city,address_street,address_house,address_apart};
            for(let key in obj){
                guest_data.push(key+'='+obj[key]);
            }
            guest_data = guest_data.join('&');
            let url = 'back/api.php?method=new_order&products='+localStorage.cart+"&"+guest_data;
            fetch(url,{
                credentials: "include",
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(x=>x.json()).then(res=>{
                if(res.ok){
                    $("#guest_data").hide();
                    clear_cart();
                    alert("Заказ успешно оформлен! Номер заказа: " + res.order_id);
                }
            });
        } else {
            alert("Заполните все необходимые поля")
        };
    });

    $('#cart_button').click(e=>{
        $('#cart').css({'top': e.target.offsetTop,'left': e.target.offsetLeft-$('#cart').width()})
            .toggle();
    });

    $('#close_cart').click(()=>{
        $('#cart').hide();
    });

    $('#search').on('change',e=>{
        let prod_search = e.target.value;
        fetch_func({prod_search});
    });

    $('.menu_item').click(e=>{
        let category = parseInt(e.target.dataset.id);
        if(category){
            $('#settings>p>b').text("Весь ассортимент > "+e.target.textContent);
        } else {
            $('#settings>p>b').text("Весь ассортимент");
        }
        fetch_func({category});
    });

    $('#prod_list').on('change','.price_select',(e)=>{
        e = e.target;
        let price = parseInt(e.dataset.price);
        let gramm = parseInt(e.value);
        $(e.parentElement).find('.item_price span').text(Math.round(price/1000*gramm));
    })
        .on('click','.prod_item .item_buy',e=>{
            e = e.target.parentElement;
            let id = parseInt(e.dataset.id);
            let name = $(e).find('.item_title').html();
            let gramm = parseInt($(e).find('select').val());
            let price = parseInt($(e).find('.item_price span').text());

            add_cart({id,name,gramm,price})
        })
        .on('click','.poi_main .item_buy',e=>{
            let id = parseInt(e.target.parentElement.parentElement.dataset.id);
            let name = $(e.target.parentElement).find('.poi_title').text();
            let gramm = parseInt($(e.target.parentElement).find('select').val());
            let price = parseInt($(e.target.parentElement).find('.item_price span').text());
            add_cart({id,name,gramm,price})
        })
        .on('click','.item_title',e=>{
            fetch_prod = [];
            let title = e.target.textContent;
            let prod_id = parseInt(e.target.parentElement.dataset.id);
            let prod_cat = parseInt(e.target.parentElement.dataset.category);
            let price = e.target.parentElement.dataset.price;
            let description = e.target.parentElement.dataset.description;
            html_one_prod(prod_id,prod_cat,title,price,description);
        });

    $('#sort_select').on('change',(e)=>{
        e = e.target;
        if(fetch_prod.length) {
            if (e.value === ">") {
                fetch_prod.sort((a, b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0));
            } else if (e.value === "<") {
                fetch_prod.sort((a, b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0));
            } else {
                fetch_prod.sort((a, b) => (parseInt(a.id) > parseInt(b.id)) ? 1 : ((parseInt(b.id) > parseInt(a.id)) ? -1 : 0));
            }
            html_prod(fetch_prod);
        }
    });

    $('#cart_list').on('click','.ci_remove',e=>{
        del_cart($(e.target.parentElement).index());
    });

    $('#cart_buy').on('click',()=>{
        fetch('back/api.php?method=new_order&products=' + localStorage.cart, {
            credentials: "include",
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(x => x.json())
        .then(res => {
            console.log(res);
            if (res.ok) {
                clear_cart();
                alert("Заказ успешно оформлен! Номер заказа: " + res.order_id);
            } else {
                if (res.err === 'missing') {
                    let miss_str = res.missing.map(x => {
                        return x.name + " - " + x.gramm + "гр.";
                    }).join(';\r\n');
                    alert("На складе не хватает товаров: " + miss_str);
                } else if (res.err === "auth_err") {
                    $('#guest_data').show();
                }
            }
        });
    });
});