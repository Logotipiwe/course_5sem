<?php

require_once '../back/db.php';

if(DB::auth($_COOKIE['loc_login'],$_COOKIE['token']) === 'user') {
    require_once 'page.php';
} else {
    header('Location: ../index.php');
}