<?php

$res = DB::query("SELECT name,surname, phone FROM users where login = '$_COOKIE[loc_login]'");
$row = $res->fetch_assoc();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="page.css" rel="stylesheet">
</head>
<body>
<script src="../libs/jquery-3.4.1.min.js"></script>
<script src="page.js"></script>
<header>
    <div id="upper">
        <a href="../">На главную</a>
        <a href="../back/api.php?method=log_out&log_out_group=user"><div>Выйти</div></a>
    </div>
    <h1>Личный кабинет пользователя <?=$row['name']." ".$row['surname']?></h1>
    <div id="headline">
    </div>
</header>
<main>
    <div class="lk_part">
        <h2>Аккаунт</h2>
        <form id="acc_form1">
            <div class="inp_div">
                <p>Имя</p>
                <input name="name" placeholder="Имя" value="<?=$row['name']?>">
            </div>
            <div class="inp_div">
                <p>Фамилия</p>
                <input name="surname" placeholder="Фамилия" value="<?=$row['surname']?>">
            </div>
            <div class="inp_div">
                <p>Телефон</p>
                <input name="phone" placeholder="Телефон" value="<?=$row['phone']?>">
            </div>
            <input type="submit" class="form_sub" value="Сохранить">
        </form>
        <form id="acc_form2">
            <p>Сменить пароль</p>
            <input type="password" name="old_pass" placeholder="Старый пароль">
            <input type="password" name="new_pass" placeholder="Новый пароль">
            <input type="password" name="new_pass2" placeholder="Подтверждение">
            <input type="submit" value="Сменить пароль">
        </form>
        <div id="close_sessions">Завершить все сеансы</div>
    </div>
    <div class="lk_part">
        <h2>История заказов</h2>
        <div id="order_story"></div>
    </div>
</main>
</body>
</html>