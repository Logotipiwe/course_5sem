

$(document).ready(()=>{

    let fetch_orders = ()=> {
        fetch('../back/api.php?method=get_order_list&loc_group=user', {
            credentials: "include",
            headers: {
                'Content-type': 'application/json'
            }
        }).then(x => x.json())
            .then(res => {
                console.log(res);
                if (Object.keys(res).length) {
                    $('.os_item').remove();
                    for (let key in res) {
                        let items = '';
                        res[key]['products'].forEach(i => items += "<div class='os_list_item'>" + i.name + " <span>" + i.gramm + "</span>гр. <span>" + i.price + "</span>р.</div>");

                        let sum = res[key]['products'].reduce((x, i) => x + parseInt(i.price), 0);


                        $('#order_story').append('<div class="os_item">' +
                            '                <div class="os_head">' +
                            '                    <h3>Номер заказа: <span>' + key + '</span></h3>' +
                            '                    <div class="os_status">Статус: <span style="color: ' + ((!res[key].status) ? 'red">Не оплачено' : 'green">Оплачено') + '</span></div>' +
                            '                </div>' +
                            '                <div class="os_list">' + items + '</div>' +
                            '                <div class="os_foo"><div>Итог: <span>' + sum + '</span>р.</div>' + ((!res[key].status) ? '<div class="close_order" data-order="' + key + '">Отменить заказ</div>' : '') + '</div>' +
                            '            </div>')
                    }
                } else {
                    $('#order_story').html("<div id='empty_os'>У вас нет заказов</div>");
                }
            });
    };
    fetch_orders();

    $( "#acc_form1" ).on( "submit", function( event ) {
        event.preventDefault();
        let str = $(this).serialize();
        fetch('../back/api.php?method=set_user_data&'+str,{
            credentials: "include"
        })
            .then(x=>x.json())
            .then(res=>{
                if(res.ok){
                    alert("Данные усешно обновлены!");
                } else {
                    if(res.err = "empty_field"){
                        alert("Заполните все поля")
                    } else {
                        alert("Ошибка")
                    }
                }
            })
    });

    $("#close_sessions").on("click",()=>{
        fetch('../back/api.php?method=exit_others_devices',{
            credentials:"include"
        })
            .then(x=>x.json())
            .then(res=>{
                if(res.ok){alert("Сеансы завершены")}
                else {alert("Ошибка")}
            })
    });

    $( "#acc_form2" ).on( "submit", function( event ) {
        event.preventDefault();
        let str = $(this).serialize();
        fetch('../back/api.php?method=set_new_password&'+str)
            .then(x=>x.json())
            .then(res=>{
                console.log(res);
                if(res.ok){
                    document.cookie = 'token='+res.new_token+"; path=/course";
                    alert('Пароль успешно изменён!')
                } else{
                    alert('Ошибка!')
                }
            });
    });

    $('#order_story').on('click','.close_order',e=>{
        let order_id = e.target.dataset.order;
        fetch('../back/api.php?method=order_del&order_id='+order_id,{
            credentials: "include",
            redirect: "follow"
        }).then(x=>x.json()).then(res=>{
            console.log(res);
            if(res.ok){
                $(e.target.parentElement.parentElement).remove();
            }
        });
        fetch_orders();
    })
});