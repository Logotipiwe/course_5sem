<?php

require_once '../back/db.php';

if(DB::auth($_COOKIE['loc_login'],$_COOKIE['token']) === 'owner'){
    require_once 'page.php';
} else {
    require_once 'login.html';
}