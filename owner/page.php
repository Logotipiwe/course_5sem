<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Кабинет владельца</title>
    <link href="page.css" rel="stylesheet">
</head>
<body>
<script src="../libs/jquery-3.4.1.min.js"></script>
<script src="page.js"></script>
<header>
    <div id="upper">
        <a href="../">На главную</a>
        <a href="../back/api.php?method=log_out"><div>Выйти</div></a>
    </div>
    <h1>Кабинет владельца <?=$_COOKIE['loc_login']?></h1>
    <div id="headline">
    </div>
</header>
<main>

    <div id="dates">
        <h2>Выбор дат:</h2>
        <input type="date" id="date1">
        <input type="date" id="date2">
        <div id="dates_sub">Выбрать</div>
    </div>
    <div>
        <div class="lk_part">
            <h2>Прибыль магазина</h2>
            <div id="money_info">
                <div id="profit" class="mi_item"><p>Прибыль</p><span></span>
                </div>
                <div id="income" class="mi_item"><p>Доход</p><span></span>
                </div>
                <div id="outcome" class="mi_item"><p>Затраты</p><span></span>
                </div>
            </div>
        </div>
        <div class="lk_part">
            <h2>Рейтинг товаров</h2>
            <div id="prod_rate_list"></div>
        </div>
    </div>
</main>

<div id="seller_manage_list">
    <div id="sm_new">
        <span>+ новый продавец</span> <input placeholder="Логин"> <input placeholder="Пароль"><input placeholder="Паспорт"><div id="sm_new_sub">Добавить</div>
    </div>
</div>

<form id="acc_form2">
    <p>Сменить пароль</p>
    <input type="password" name="old_pass" placeholder="Старый пароль">
    <input type="password" name="new_pass" placeholder="Новый пароль">
    <input type="password" name="new_pass2" placeholder="Подтверждение">
    <input type="submit" value="Сменить пароль">
    <div id="close_sessions">Завершить все сеансы</div>
</form>

</body>
</html>