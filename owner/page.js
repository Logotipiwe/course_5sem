

$(document).ready(()=>{

    let get_analytics = ()=>{
        let date1 = $('#date1').val();
        let date2 = $('#date2').val();

        fetch('../back/api.php?method=get_owner_data&date1='+date1+'&date2='+date2,{
            credentials: "include"
        }).then(x=>x.json()).then(res=>{
            console.log(res);
            if(res.ok){
                let rate = res.ans.rate;
                $('#prod_rate_list').html(rate.map(item=>
                    "<div class='pr_item'>" +
                    "   <h2>"+item.name+" <span>("+item.profit+"р)</span></h2>" +
                    "</div>"
                ));

                let money = res.ans.money;
                let profit = parseInt(money.income)-parseInt(money.outcome);
                $("#profit span").text(profit+'р.');
                $("#income span").text(money.income+'р.');
                $("#outcome span").text(money.outcome+'р.');

                let sellers = res.ans.sellers;
                $("#seller_manage_list").html(sellers.map(item=>
                    "<div class='sm_item' data-id='"+item.id+"'>" +
                    "   <h3>'"+item.login+"' "+item.name+' '+item.surname+"  "+item.passport+"</h3>" +
                    "   <div class='sm_item_del'>&#216;</div>" +
                    "</div>"
                ));

                $("#seller_manage_list").prepend(
                    "<div id='sm_new'>" +
                    "   <span>+ новый продавец</span> <input placeholder='Логин'> <input placeholder='Пароль'> <input placeholder='Паспорт'> <div id='sm_new_sub'>Добавить</div>" +
                    "</div>"
                );
            }
        })
    };

    get_analytics();

    $("#close_sessions").on("click",()=>{
        fetch('../back/api.php?method=exit_others_devices',{
            credentials:"include"
        })
            .then(x=>x.json())
            .then(res=>{
                if(res.ok){alert("Сеансы завершены")}
                else {alert("Ошибка")}
            })
    });

    $( "#acc_form2" ).on( "submit", function( event ) {
        event.preventDefault();
        let str = $(this).serialize();
        fetch('../back/api.php?method=set_new_password&'+str)
            .then(x=>x.json())
            .then(res=>{
                console.log(res);
                if(res.ok){
                    document.cookie = 'token='+res.new_token+"; path=/course";
                    alert('Пароль успешно изменён!')
                } else{
                    alert('Ошибка!')
                }
            });
    });

    $('#dates_sub').click(get_analytics);

    $("#seller_manage_list").on('click','.sm_item_del',e=>{
        let id = e.target.parentElement.dataset.id;
        fetch('../back/api.php?method=seller_delete&id='+id,{
            credentials: "include"
        }).then(get_analytics);
    });

    $('#seller_manage_list').on('click','#sm_new_sub',e=>{
        let login = $(e.target.parentElement).find('input:nth-child(2)').val();
        let password = $(e.target.parentElement).find('input:nth-child(3)').val();
        let passport = $(e.target.parentElement).find('input:nth-child(4)').val();
        fetch('../back/api.php?method=new_seller&login='+login+'&password='+password+'&passport='+passport,{
            credentials: "include"
        }).then(x=>x.json()).then(res=>{
            get_analytics();
        });
    });
});