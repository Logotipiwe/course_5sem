<?php
require_once 'back/db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="index.css" rel="stylesheet">
    <script src="libs/jquery-3.4.1.min.js"></script>
    <script src="index.js"></script>
</head>
<body>
    <div id="guest_data">
        <div id="gd_main">
            <h2>Введите данные для заказа</h2>
            <div id="gd_fields">
                <div id="gd_titles">
                    <div>Имя*</div>
                    <div>Фамилия*</div>
                    <div>Email*</div>
                    <div>Дата р.</div>
                    <div>Телефон*</div>
                    <div>Адрес</div>
                </div>
                <div id="gd_inputs">
                    <input id="gd_inp_name" placeholder="Имя">
                    <input id="gd_inp_surname" placeholder="Фамилия">
                    <input id="gd_inp_email" placeholder="Email">
                    <input id="gd_inp_bdate" type="date">
                    <input id="gd_inp_phone" placeholder="Телефон">
                    <input id="gd_inp_address_city" placeholder="Город">
                    <div>
                        <input id="gd_inp_address_street" placeholder="Улица">
                        <input id="gd_inp_address_house" placeholder="Дом">
                        <input id="gd_inp_address_apart" placeholder="Кв.">
                    </div>
                </div>
            </div>
            <div id="gd_submit">Оформить заказ</div>
        </div>
    </div>
    <div id="cart">
        <div id="cart_head"><span>Корзина</span><div id="close_cart">&#216;</div></div>
        <div id="cart_list"></div>
        <div id="cart_buy">Оформить заказ (<span></span>р.)</div>
    </div>
    <div>
        <div id="upper">
            <?php if(DB::auth($_COOKIE['loc_login'],$_COOKIE['token']) !== 'user'):?>
            <div><a href="login">Личный кабинет</a></div>
            <?php else: ?>
            <div>Вы вошли, как <a href="user" style="margin-left: 5px"><?=$_COOKIE['loc_login']?></a></div>
            <?php endif;?>
        </div>
        <div id="logo">tea.ru <span>магазин китайского чая</span></div>
        <header>
            <input placeholder="Поиск товаров" id="search">
            <div id="cart_button">Корзина (<span>0</span>)</div>
        </header>
        <main>
            <div id="menu">
                <div class="menu_item" data-id="0">Весь ассортимент</div>
                <?php
                $res = DB::query("SELECT * FROM categories");
                while($row = $res->fetch_assoc()){
                    echo "<div class='menu_item' data-id='$row[id]'>$row[title]</div>";
                }
                ?>
            </div>
            <div id="main">
                <div id="main_img">У нас лучший чай</div>
                <div id="products">
                    <div id="settings">
                        <p><b>Наши товары</b></p>
                        <div id="sort">
                            Сортировка:
                            <select id="sort_select">
                                <option selected value="0">По умолчанию</option>
                                <option value=">">По возрастарию цены</option>
                                <option value="<">По убыванию цены</option>
                            </select>
                        </div>
                    </div>
                    <div id="prod_list"></div>
                </div>
            </div>
        </main>
        <div id="footer">
            <div id="f_list">
                <a href="seller">Сотрудникам</a>
                <a href="owner">Владельцам</a>
            </div>
        </div>
    </div>
</body>
</html>