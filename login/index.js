$(document).ready(function () {
    $('#log_button').click(() => {
        $('#slider').css('margin-left', '0');
        $('#sign_up').hide();
        $('#sign_in').show();
    });
    $('#reg_button').click(() => {
        $('#slider').css('margin-left', '50%');
        $('#sign_in').hide();
        $('#sign_up').show();
    });
    $('#sign_in input').on('input', () => {
        let login = $('#in_login').val();
        let pass = $('#in_pass').val();

        if (login !== '' && pass !== '') {
            $('#in_sub').prop('disabled', false);
        } else {
            $('#in_sub').prop('disabled', true);
        }
    });
    $('#sign_up input[type=submit]').click(e => {
        e.preventDefault();
        let login = $('#up_login').val();

        fetch('../back/api.php?method=check_login&login=' + login)
            .then(x => x.json())
            .then(res => {
                if (res.ok && res.success) {
                    $('#sign_up').submit();
                } else console.log(res.err_msg);
            })
    });
    $('#sign_up input').on('input', () => {
        if (
            // $('#up_name').val() !== '' &&
        // $('#up_surname').val() !== '' &&
            $('#up_login').val() !== '' &&
            $('#up_pass').val() !== '' &&
            // $('#up_phone').val() !== '' &&
            // $('#up_email').val() !== '' &&
            // $('#up_address_city').val() !== '' &&
            // $('#up_address_street').val() !== '' &&
            // $('#up_address_house').val() !== '' &&
            // $('#up_address_apart').val() !== '' &&
            $('#up_pass').val() === $('#up_pass2').val()
        ) {
            $('#up_sub').prop('disabled', false);
        } else {
            $('#up_sub').prop('disabled', true);
        }
        if ($('#up_pass').val() !== $('#up_pass2').val()) {
            $('#up_pass2').css('border', '1px red solid')
        } else {
            $('#up_pass2').css('border', 'none');
        }
    });
    $("#up_address_city").suggestions({
        token: "7922f91ac0a5c2514a1d6ef788441486eb43e6fd",
        type: "ADDRESS",
        onSelect: function (suggestion) {
            $('#up_address_city').val(suggestion.data.city);
            $('#up_address_street').val(suggestion.data.street);
            $('#up_address_house').val(suggestion.data.house);
        }
    });
    $('#up_login').change(e => {
        fetch('../back/api.php?method=check_login&login=' + $(e.target).val())
            .then(x => x.json())
            .then(res => {
                if (res.ok) {
                    if (!res.success) {
                        $(e.target).css('border', '1px red solid');
                        $(e.target.parentElement).find('p').remove();
                        $(e.target).after("<p style='margin: -10px 0 10px;font-size: 15px;font-weight: 900;color: red;'>Такой логин уже зарегистрирован!</p>")
                    } else {
                        $(e.target).css('border', 'none');
                        $(e.target.parentElement).find('p').remove();
                    }
                } else {
                    alert(res.err_msg);
                }
            })
    })
    $('#in_sub').on('click', e => {
        e.preventDefault();
        let login = $('#in_login').val();
        let password = $('#in_pass').val();
        fetch('../back/api.php?method=sign_in_check&login=' + login + '&pass=' + password, {
            credentials: "include"
        }).then(x => x.json())
            .then(res => {
                if (res.ok) {
                    $('#sign_in').submit();
                } else alert('Неверный логин или пароль');
            })
    })
});